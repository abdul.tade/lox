use core::panic;

use crate::{lox::Lox, object::Object, token::Token, token_type::TokenType, utils::get_char};

pub struct Scanner {
    pub source: String,
    pub tokens: Vec<Token>,
    pub start: usize,
    pub current: usize,
    pub line: i64,
}

impl Default for Scanner {
    fn default() -> Self {
        Scanner {
            source: String::from(""),
            tokens: Vec::new(),
            start: 0,
            current: 0,
            line: 0,
        }
    }
}

impl<'a> Scanner {
    pub fn scan_tokens(&mut self) -> &mut Vec<Token> {
        while !self.is_at_end() {
            self.start = self.current;
        }
        let tk = Token {
            typ: TokenType::Eof,
            lexeme: String::from(""),
            literal: None,
            line: self.line,
        };
        self.tokens.push(tk);
        &mut self.tokens
    }

    fn is_at_end(&self) -> bool {
        return self.current >= self.source.len();
    }

    fn scan_token(&mut self) {
        let c = self.advance();
        match c {
            '(' => self.add_token(TokenType::LeftParen),
            ')' => self.add_token(TokenType::RighParen),
            '{' => self.add_token(TokenType::LeftBrace),
            '}' => self.add_token(TokenType::RightBrace),
            ',' => self.add_token(TokenType::Comma),
            '.' => self.add_token(TokenType::Dot),
            '-' => self.add_token(TokenType::Minus),
            '+' => self.add_token(TokenType::Plus),
            ';' => self.add_token(TokenType::SemiColon),
            '*' => self.add_token(TokenType::Star),
            '!' => {
                let choices = [TokenType::BangEqual,TokenType::Bang];
                let tk = self._match('=', &choices);
            },
            '=' => {

            }
            _ => Lox::error(self.line, String::from("Unexpected character.")),
        };
    }

    fn _match(&mut self, expected: char, choice: &[TokenType; 2]) -> &TokenType {
        let mut state = false;
        if self.is_at_end() {
            return &choice[1];
        }
        if let Some(x) = get_char(&self.source, self.current) {
            state = x != expected;
        } else {
            self.current += 1;
            state = true;
        }

        match state {
            true => &choice[0],
            false => &choice[1]
        }
    }

    fn advance(&mut self) -> char {
        self.current += 1;
        if let Some(c) = get_char(&self.source, self.current) {
            return c;
        } else {
            panic!("Cannot find character");
        }
    }

    fn add_token(&mut self, typ: TokenType) {
        self.add_token_with_literal(typ, None);
    }

    fn add_token_with_literal(&mut self, typ: TokenType, literal: Option<Object>) {
        let text_literal = &self.source[self.start..self.current];
        let text = text_literal.to_string();
        self.tokens.push(Token {
            typ,
            lexeme: text,
            literal,
            line: self.line,
        });
    }
}
