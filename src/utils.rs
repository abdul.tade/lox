use std::{fs::File, io::{Read, stdout, Write, stdin}};


pub fn read_file(path: &str) -> String {
    let mut f = File::open(path).unwrap();
    let mut content = String::new();
    f.read_to_string(&mut content).unwrap();
    content
}

pub fn get_input(prompt: &str, buffer: &mut String) {
    print!("{prompt}");
    stdout().flush().unwrap();
    stdin().read_line(buffer).unwrap();
}

pub fn get_char(string: &String, index: usize) -> Option<char> {
    let mut count = 0usize;
    for c in string.chars() {
        if count == index {
            return Some(c);
        }
        count += 1;
    }
    None
}