use std::{process::exit, io::{stderr, Write}};

use crate::{utils, scanner::Scanner, token::Token};


pub struct Lox {
    had_error: bool
}

impl Default for Lox {
    fn default() -> Self {
        Lox {
            had_error: false
        }
    }
}

impl Lox {
    pub fn main(&mut self, args: Vec<&str>) {
        if args.len() > 1 {
            println!("Usage lox [script] ");
            exit(64);
        } else if args.len() == 1 {
            self.run_file(args[0]);
        } else {
            self.run_prompt();
        }
    }

    fn run_file(&mut self, path: &str) {
        let content = utils::read_file(path);
        self.run(&content);
        if self.had_error {
            exit(65);
        }
    }

    fn run_prompt(&mut self) {
        let mut line = String::new();
        loop {
            utils::get_input("> ", &mut line);
            if line == "" {
                break;
            }
            self.run(&line);
            self.had_error = false;
        }
    }

    fn run(&mut self, source: &String) {
        let mut scanner = Scanner{source: source.to_string(), ..Default::default() };
        let tokens = scanner.scan_tokens();

        for token in tokens {
            println!("{token}");
        }
    }

    fn report(&mut self, line: i64, whr: String, message: String)  {
        let err_msg = format!("[line {line}] Error {whr}: {message}");
        stderr().write_all(err_msg.as_bytes()).unwrap();
        stderr().flush().unwrap();
        self.had_error = true;
    }

    pub fn error(line: i64, message: String) {
        let mut lox = Lox{ had_error: false };
        lox.report(line, String::from(""), message);
    }
}
