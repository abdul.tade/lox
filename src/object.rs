#[derive(Debug)]
pub struct Object {
    pub int: i64,
    pub float: f64,
    pub string: String,
}

impl Default for Object {
    fn default() -> Self {
        Object {
            int: 0,
            float: 0.0,
            string: String::from(""),
        }
    }
}
