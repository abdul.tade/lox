use std::fmt::Display;

use crate::{token_type::TokenType, object::Object};

pub struct Token {
    pub typ: TokenType,
    pub lexeme: String,
    pub literal: Option<Object>,
    pub line: i64
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?} {} {:?}",self.typ, self.lexeme, self.literal))
    }
}

